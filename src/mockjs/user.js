import Mock from 'mockjs'
import dataBox from './userPassword.js'

export default {
    // 模拟用户登录
    login: config => {
        let data = JSON.parse(config.body);
        let userList = {};
        let bel = dataBox.data.data.map(res => {
            if (data.username === res.username && data.password === res.password) {
                userList = res.userList
                return true
            } else {
                return false
            }
        })
        if (bel.includes(true)) {
            return {
                code: 200,
                data: {
                    userList
                }
            }
        } else {
            return {
                code: -1,
                data: {
                    data: {
                        userList: {
                            msg: "用户名或者密码错误！",
                            status: "fail",
                        },
                    },
                    code: -1,
                }
            }
        }
    }
}