import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
// import Home from '../views/Home.vue'
import Home from '../views/NextTick.vue'
import Login from '../views/Login.vue'
import Video from '../views/video.vue'
import Echarts from '../views/Echarts.vue'
import EventLoop from '../views/EventLoop.vue'
import NextTick from '../views/NextTick.vue'
import Layout from '../components/Layout.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/',
        redirect: '/login'
     },
    {
       path: '/login',
       component: Login
    },
    {
        path: '/home',
        name: 'home',
        // component: Layout,
        component: Home,
    },
    {
        path: '/video',
        name: 'video',
        component: Video,
    },
    {
        path: '/echarts',
        name: 'echarts',
        component: Echarts,
    },
    {
        path: '/eventLoop',
        name: 'eventLoop',
        component: EventLoop,
    },
    {
        path: '/nextTick',
        name: 'nextTick',
        component: NextTick
    }
]

const router = new VueRouter({
    routes,
  });
  
export default router;