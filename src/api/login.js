import request from '../utils/request.js'

export const loginApi = (params) => {
    return request({
      url: '/user/login',
      method: 'post',
      data: params
    })
  }