module.exports = {
    // 配置跨域请求
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    devServer: {
        port: 8088,
        host: 'localhost',
        open: true,
        https: false,
            proxy: {
                '/api': {
                    target: 'http://api.vikingship.xyz/',
                    ws: true,
                    changeOrigin: true,
                    pathRewrite: {
                    '^/api': '/api'
                    }
                }
            }
    }
}