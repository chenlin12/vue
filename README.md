![Vue.js Logo](https://github.com/vercel/vercel/blob/master/packages/frameworks/logos/vue.svg)

# Vue.js Example

This directory is a brief example of a [Vue.js](https://vuejs.org/) app that can be deployed with Vercel and zero configuration.

## Deploy Your Own

Deploy your own Vue.js project with Vercel.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/import/project?template=https://github.com/vercel/vercel/tree/main/examples/vue)

_Live Example: https://vue.now-examples.now.sh_

### How We Created This Example

To get started with Vue.js deployed with Vercel, you can use the [Vue CLI](https://cli.vuejs.org/guide/creating-a-project.html#vue-create) to initialize the project:

```shell
$ vue create
```

### Deploying From Your Terminal

You can deploy your new Vue.js project with a single command from your terminal using [Vercel CLI](https://vercel.com/download):

```shell
$ vercel
```
### 1.https://xiehao.blog.csdn.net/article/details/89920379
### 账号：111@test.com 密码：111111

### 访问：https://vue-inky-iota.vercel.app/#/login
### 代码地址：https://gitlab.com/chenlin12/vue